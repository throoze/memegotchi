package com.memegotchi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class GameMenu extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.gamemenu);
	}
	
	public void continueGame(View view) {
		Intent intent = new Intent(this, GamePlay.class);
		startActivity(intent);
	}

}
