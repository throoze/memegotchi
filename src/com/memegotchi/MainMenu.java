package com.memegotchi;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainMenu extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mainmenu);
	}

	public void createMeme(View view) {
		Intent intent = new Intent(this, CreateMeme.class);
		startActivity(intent);
	}

	public void loadMeme(View view) {
		Intent intent = new Intent(this, LoadMeme.class);
		startActivity(intent);
	}

	public void exitApp(View view) {
		// ??????????????????????????????????????
	}
}
