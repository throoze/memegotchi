package com.memegotchi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class CreateMeme extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
        setContentView(R.layout.creatememe);
	}
	
	public void done(View view) {
		Intent intent = new Intent(this, GamePlay.class);
		startActivity(intent);
	}

}
