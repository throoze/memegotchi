package com.memegotchi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MemegotchiActivity extends Activity {
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

	public void start(View view) {
		Intent intent = new Intent(this, MainMenu.class);
		startActivity(intent);
	}
}